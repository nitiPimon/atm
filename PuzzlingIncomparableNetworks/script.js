var account = {
    '1234' : {'pin': '1234', 'balance': 500},
    '5678' : {'pin': '5678', 'balance': 1500},
    '0000' : {'pin': '0000', 'balance': 2500}
  };
var accountNo;
$(document).ready(function() {

  $('.form-menu').hide();
  $('.form-deposit').hide();
  $('.form-withdraw').hide();
  $('.form-thanks').hide();

  $('.form-signin').submit(function() {
    accountNo = $('#accountNo').val();
    var pin = $('#pin').val();
    if (accountNo != null && account[accountNo]['pin'] == pin) {
      $('.form-menu').show();
      $('.form-signin').hide();
      showbalance(accountNo);
    } else {
      alert('เลขที่บัญชีหรือรหัสประจำตัวไม่ถูกต้อง');
    }
    return false;
  })

  $('#dep').click(function() {
    $('.form-menu').hide();
    $('.form-deposit').show();
    return false;
  })

  $('.form-deposit').submit(function() {
    var amount = $('#deposit').val();
    amount = parseInt(amount,10);

    if (amount >= 0) {
      deposit(accountNo, amount);
      $('.form-deposit').hide();
      $('.form-thanks').show();
      showbalance(accountNo);
    } else {
      alert('กรุณากรอกตัวเลขให้ถูกต้อง');
    }
    return false;
  })

  $('#wit').click(function() {
    $('.form-menu').hide();
    $('.form-withdraw').show();
    return false;
  })

  $('.form-withdraw').submit(function() {
    var amount = $('#withdraw').val();
    amount = parseInt(amount,10);
    
    if (amount < 0){
      alert('กรุณากรอกตัวเลขให้ถูกต้อง');
    }
    else{
      if (amount <= account[accountNo]['balance']) {
        $('.form-withdraw').hide();
        $('.form-thanks').show();
        withdraw(accountNo, amount);
        showbalance(accountNo);
      } else {
        alert('กรุณาตรวจสอบยอดเงินก่อนทำรายการ');
      }
    }
    return false;
  })
  $("#exit").click(function(){
    $(".form-signin").show();
    $(".form-menu").hide();
  });
  $("#cancel").click(function(){
    $(".form-menu").show();
    $(".form-deposit").hide();
  });
  $("#cancel1").click(function(){
    $(".form-menu").show();
    $(".form-withdraw").hide();
  });

  function showbalance(accountNo) {
    $(".balance").html(account[accountNo]['balance'].toString())
  }
  function deposit(accountNo, amount) {
    account[accountNo]['balance'] += amount
  }
  function withdraw(accountNo, amount) {
    account[accountNo]['balance'] -= amount
  }
})